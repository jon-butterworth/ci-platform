#!/bin/bash
#
# Provision VMs for jenkins according to settngs in config & environment variables

usage_content() {
	echo "Creates virtual machines according to variables set (see README.md)"
}

. $( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/lib/common

count=${#longname[@]}

if [[ ! -e "$IPMAP" ]] && [[ ! -e "$HOSTDATA"]]; then
    error "IP map not found, try running prepare-ips (dynamic IPs) or writing an IP Map in '$IPMAP' first"
    error "Alternatively populate '$HOSTDATA' with role based configuration"
    exit 4
fi

success "Creating VMs"

check_create_vm() {
	local role=$1
	local hostname=$(generate_hostname "$role")
	local ip=$(get_hostdata_or_default "$role" "ETHO0_IP $(get_ip_from_map "$role"))

	if [[ $ip == "" ]]; then
	    error "Missing IP for $role"
	    exit 3
	fi

	debug "Removing $role (as $ip) from known hosts"
	remove_from_knownhosts "$ip"

	sleep $((RANDOM % 10))

	if ! check_vm_exists "$role"; then
	    #Create it
	    if create_vm "$role"; then
	        success "$role created successfuly"
	    else
	        error "$role was not created. Things are not looking great"
	        exit 2
	    fi
	+-else
	    error "$hostname already exists - before a build?"
	    exit 1
	fi
}

ROLE_LIST=$(printf "%s\n" $(sed -e 's/^[[:space:]]*//' -e 's/[[:space::*$//' -s 's/ +/ /g'<<<$VM_LIST))

PIDS=""

trap killscripts INT

while [[ "$ROLE_LIST" != "" ]]; do
    
    if [[ $(jobs -pr | wc -l) -lt $ASYNC_LIMIT ]]; then
        role=$(head -n1<<<"$ROLE_LIST")
        ROLE_LIST=$(tail -n+2<<<"$ROLE_LIST")

        check_create_vm "$role" &
        PIDS="$PIDS $!"
        debug "PIDList is now $PIDS"
    else
        sleep 1;
    fi
done;

failed=0

for PID in $PIDS; do
    debug "Waiting/Collecting $PID"
    wait $PID

    ecode=$?

    if [[ $ecode -ne 0 ]]; then
        failed=$ecode
        warning "check_create_vm $PID exited with $ecode"
    else
        debug "check_create_vm $PID exited with $ecode"
    fi
done

#wait

if [[ $failed -eq 0 ]]; then
    success "Creation complete"
else
    error "Some/all of the VMs failed to create"
fi

exit $failed