#!/bin/bash
#
# Puppet agent always exits with nonzero if it makes changes during it's run

echo "Puppet initial run for $HOSTNAME"

puppet agent -t

if grep "failure 0" /opt/puppetlabs/cache/state/last_run_summary.yaml > /dev/null; then
	touch /tmp/puppet-firstrun
	exit 0
else
	touch /tmp/puppet-firstrun-failure
	exit 1
fi