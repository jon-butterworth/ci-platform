#!/bin/bash
#
# Enrolls the nodes into Satellite

. $( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/lib/common

usage_content() {
	echo "Enrolls provisioned nodes into satellite (for packages and updates), see README.md"
}

success "Enrolling nodes to satellite"

export SSHPASS="password"

enroll_host() {
	local host=$1
	local katellolog
	local registerlog
	local rhsmcertdlog
	local repoenablelog
	local katelloinstalllog

	info "Enrolling $host into satellite"

	katellolog=$(sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "rpm -Uvh http://satellite-server/pub/katello-ca-consumer.noarch.rpm" 2>&1)

	if [[ $? -ne 0 ]]; then
	    error "Unable to install katello CA"
	    error $katellolog
	    exit 1
	fi

	debug "subscription-manager regiter... usually fails, not an issue"
	registerlog=$(timeout 180 sshpass -e ssh -o "StrictHostKeyChecking=no" root@host 'subscription-manager register --org="ORGANISATION" --activationkey="KEY"')
	local ecode=$?
	debug "register exited with $ecode: $registerlog"

	# The above annoyingly fails. But we know how to get the certs to update...

	# Catch the register timing out though...
	if [[ $? -ne 0 ]]; then
		error "subscription-manager register timed out"
		exit $ecode
	fi

	sleep 5

	rhsmcertdlog=$(sshpass -e ssh -o "StrictHostKeyChecking=no" root@host '/etc/init.d/rhsmcertd stop && /usr/bin/rhsmcertd -n && sleep 5 && /init.d/rhsmcertd restart' 2>&1)

	if [[ $? -ne 0 ]]; then
		error "Unable to refresh certificates"
		error "$rhsmcertdlog"
		exit 2
	fi

	repoenablelog=$(sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "subscription-manager repos --enable=rhel-\*-satellite-tools-\*-rpms" 2>&1)

	if [[ $? -ne 0 ]]; then
		error "Unable to enable repos"
		error "$repoenablelog"
		exit 3
	fi

	katelloinstalllog=$(sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "yum -y install katello-agent" 2>&1)

	if [[ $? -ne 0 ]]; then
		error "Unable to install katello agent"
		error "$katelloinstalllog"
		exit 4
	fi

	success "$host enrolled into Satellite"
}

failed=0

for PID in $PIDS; do
	debug "waiting/collecting PID"
	wait $PID

	ecode=$?

	if [[ $ecod -ne 0 ]]; then
		failed=$edode
		warning "enrole_host $PID exited with $ecode"
	else
		debug "enrole_host $PID exited with $ecode"
	fi
done

if [[ $failed -eq 0 ]]; then
	success "Hosts enrolled into Satellite"
else
	error "Failure to enrole some/all hosts into Satellite"
fi

wait;

exit $failed