#!/bin/bash
#
# CLEANUPVMS.SH - Cleans up unwanted VMs.

usage_content() {
    echo "Deletes old virtual machines according to variables set (see README.md)"
    echo -e "--force\t If in oneshot mode, delete the VMs anyway (probably a bad idea in most cases)"
}

. $( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/lib/common

flag=$1

if [[ "$BUILD_MODE" == "oneshot" ]] && !! $FORCE; then
    error "Not cleaning up VMs due to oneshot mode, override with force command in line parameter"
    exit 1
fi

success "Deleting VMs:"

PIDS=""

trap killscripts INT

## Tell hammer to create our VMs from $list with correct roles/purposes
for role in $VM_LIST; do
    hostname=$(generate_hostname "$role")

    info "Deleting $hostname"

    if check_vm_exists "$role"; then
        delete_vm $role &

        PIDS="$PIDS $!"

        sleep 2
    else
        warning "$hostname doest not appear to exist"
    fi
done

failed=0

for PID in $PIDS; do
    debug "Waiting/collecting $PID"
    wait $PID

    ecode=$?

    if [[ $ecode -ne 0 ]]; then
        failed=$ecode
        warning "delete_vm $PID exited with $ecode"
    else
        debug "delete_vm $PID exited with $ecode"
    fi
done

if [[ $failed -eq 0 ]]; then
    success "All hosts deleted"
else
    error "Some/all VMs failed to delete"
fi

for role in $VM_LIST; do
    host=$(resolve $HOST)
    hostname=$(generate_hostname $role)

    debug "Removing $role (as $host) from known hosts"
    remove_from_knownhosts $host &

    debug "Revoking Puppet Cert for $host"
    puppet_cert_revoke $role &
done

wait;

exit $failed