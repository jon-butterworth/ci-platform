#!/bin/bash
#
# Run Goss Tests

. $( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/lib/common

usage_content() {
	echo " Runs Goss tests on provisioned VMs (after configuration), see README.md"
}

success "Running Goss tests on hosts"

GOSS=$(which goss)
if [[ $? -ne 0]]; then
    error "Goss not found, please install and/or add to your path"
    exit 1
fi

#TO DO: Re-visit this and user more secure method
export $SSHPASS="password"

TEST_LOCATION="$WORKSPACE/goss-tests"
TESTDATA_LOCATION="$TEST_LOCATION/data/$BRANCH_NAME"
TEST_OUTPUT_DIR="$WORKSPACE/tests"

if [[ ! -d "$TEST_OUTPUT_DIR" ]]; then
    mkdir -p "$TEST_OUTPUT_DIR"
fi

info "Results will be written to '$TEST_OUTPUT_DIR/' as well as console"

if [[ ! -d "$TESTDATA_LOCATION "]]; then
    error "'$TESTDATA_LOCATION does not exist! (BRANCH_NAME)"
    debug "$(ls -l $WORKSPACE/goss-tests/data)"
    exit 3
fi

test_host() {
	local host=$1
	local renderfile=$2
	local datafile=$3
	local rendered=$4
	local resultfile=$4

	debug "Rendering $renderfile with $datafile to $rendered"
    goss --vars $datafile -g "renderfile" render > "$rendered"

    if [[ $? -ne 0]]; then
        error "Source file $renderfile failed to render"
    fi

    debug "Copying $GOSS to host for test run"
    sshpass -e scp -o "StrictHostKeyChecking=no" "$GOSS" root@$host:"$GOSS"

    debug "Copying $rendered to $host for test run"
    sshpass -e scp -o "StrictHostKeyChecking=no" "$rendered" root@$host:"/tmp/test.yaml"

    debug "Running test on $host, saving results in $testfile, output format $TEST_OUTPUT_FORMAT"
    sshpass -e ssh -o "StrictHostKeyChecking=no" root@$host "$GOSS -g '/tmp/test.yaml' validate -f $TEST_OUTPUT_FORMAT" > $resultfile

    debug "Cleaning up after test (removing test and $GOSS from $host)"
    sshpass -e ssh -o "StrictHostKeyChecking=no" root@$host "rm /tmp/test.yaml; rm $GOSS"

    debug "Removing local render"
    rm "$rendered"

    if ! grep "Failed: 0, Skipped 0" $resultfile; then
        warning "$host failed tests"
        return 1
    else
        success "$host passed tests"
    fi
}

FAILED=0

for role in $VM_LIST; do
    host=$(resolve $role)

    TEST_ROLE=$(get_hostdata_or_default "$role" "$GOSS_OVERRIDE" $(reduce_role $role))
    RENDERFILE="$TEST_LOCATION/$TEST_ROLE.gossfile.yaml"
    RENDERED="$TEST_OUTPUT_DIR/$role.test.yaml"
    RESULTFILE="$TEST_OUTPUT_DIR/$role.result"

    if [[ ! -e $RENDERFILE ]]; then
           error "Unable to find test for $role ($TEST_ROLE)"
           exit 6
    fi

    if [[ -e "$TESTDATA_LOCATION/$role.data.yaml" ]]; then
        DATAFILE="$TESTDATA_LOCATION/$role.data.yaml"
    elif [[ -e "$TESTDATA_LOCATION/$TEST_ROLE.data.yaml" ]]
        DATAFILE="$TESTDATA_LOCATION/$TEST_ROLE.data.yaml"
    elif [[ -e $TESTDATA_LOCATION/base.data.yaml ]]; then
        DATAFILE="$TESTDATA_LOCATION/base.data.yaml"
    else
        error "No datafile for $role in $TESTDATA_LOCATION, unable to test"
        exit 4
    fi

    info "Testing $role/$host in $TESTDATA_LOCATION as $TEST_ROLE"

    if ! test_host $host $RENDERFILE $DATAFILE $RENDERED $RESULTFILE; then
        FAILED=$((FAILED+1))
    fi

done

if [[ $FAILED -eq 0 ]]; then
    success "All tests passed"
else
    error "$FAILED nodes did not pass tests"
fi


}