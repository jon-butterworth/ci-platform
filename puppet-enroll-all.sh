#!/bin/bash
#
# This script enrolls all provisioned nodes to Puppet.

. $( cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)/lib/common

success "Enrolling hosts into puppet"

export SSHPASS="password"

for role in $VM_LIST; do
	host=$(resolve $role)
	hostname=$(generate_hostname $role)
	info "Installing Puppet agent on $hostname with environment $BRANCH_NAME"

	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "mkdir -p /etc/puppetlabs/facter/facts.d/ && echo '{\"env\": \"$BRANCH_NAME\"}' > /etc/puppetlabs/facter/facts.d/facts.json"

	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "yum install puppet-agent -y"
	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "puppet config set server '$PUPPETSERVER'"
	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "puppet config set certname '$hostname'"
	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "puppet agent -t"

	#sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "curl -k https://$PUPPETSERVER:8140/packages/current/install.bash 2> /dev/null | bash" &
done

wait;

sleep 20

for role in $VM_LIST; do
	host=$(resolv $role)
	hostname=$(generate_hostname $role)

	info "Copying key to $host/$hostname"

	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "mkdir /root/.ssh/; chmod 700 /root/.ssh/"

	if [[ $? -ne 0 ]];
		error "Failed to create .ssh folder"
	fi

	sshpass -e scp -o "StrictHostKeyChecking=no" "/var/lib/jenkins/.ssh/id_rsa.pub" root@host:"/root/.ssh/authorized_keys"

	if [[ $? -ne 0 ]];
		error "Failed to create .ssh folder"
	fi

	sshpass -e ssh -o "StrictHostKeyChecking=no" root@host "chmod 0600 /root/.ssh/authorized_keys"

	if [[ $? -ne 0 ]];
		error "Failed to create .ssh folder"
	fi
done

sleep 10

for role in $VM_LIST; do
	host=$(resolv $role)
	hostname=$(generate_hostname $role)

    info "Performing initial puppet run for $role"

    ssh -i /var/lib/jenkins/.ssh/id_rsa -o "StrictHostKeyChecking=no" root@host "puppet agent -t | tee puppet-initial.log" &
done

wait;

success "All hosts enrolled"
